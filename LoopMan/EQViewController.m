//
//  EQViewController.m
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "EQViewController.h"
#import "LHKnobControl.h"
#import "LHLabel.h"

@interface EQViewController ()

@property (weak, nonatomic) IBOutlet LHKnobControl *frequencyKnob;
@property (weak, nonatomic) IBOutlet LHKnobControl *bandwidthKnob;
@property (weak, nonatomic) IBOutlet LHKnobControl *gainKnob;

@property (weak, nonatomic) IBOutlet LHLabel *freqLabel;
@property (weak, nonatomic) IBOutlet LHLabel *qLabel;
@property (weak, nonatomic) IBOutlet LHLabel *gainLabel;


@property (nonatomic) NSInteger band;

@end

@implementation EQViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"equalizer", nil);
    
    for (LHKnobControl *knob in @[self.frequencyKnob, self.bandwidthKnob, self.gainKnob])
    {
        [knob setImage:[UIImage imageNamed:@"knob2.png"]];
        knob.imageWidth = 64;
        knob.imageHeight = 64;
        knob.frameCount = 31;
        [knob setBackgroundColor:[UIColor clearColor]];
    }
    
    self.gainKnob.minimumValue = -60;
    self.gainKnob.maximumValue = 12;
    self.bandwidthKnob.minimumValue = 0.0;
    self.bandwidthKnob.maximumValue = 4.0;
    
    self.freqLabel.defaultText = NSLocalizedString(@"frequency", nil);
    self.freqLabel.timeout = 1;
    self.qLabel.defaultText = NSLocalizedString(@"Q", nil);
    self.qLabel.timeout = 1;
    self.gainLabel.defaultText = NSLocalizedString(@"gain", nil);
    self.gainLabel.timeout = 1;
    
    self.band = 0;
    [self configureKnobsForBand:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureKnobsForBand:(NSInteger)band
{
    switch (band)
    {
        case 0:
            self.frequencyKnob.minimumValue = 20;
            self.frequencyKnob.maximumValue = 250;
            break;
            
        case 1:
            self.frequencyKnob.minimumValue = 250;
            self.frequencyKnob.maximumValue = 2000;
            break;
            
        case 2:
            self.frequencyKnob.minimumValue = 2000;
            self.frequencyKnob.maximumValue = 4000;
            break;
            
        case 3:
            self.frequencyKnob.minimumValue = 4000;
            self.frequencyKnob.maximumValue = 10000;
            break;
            
        default:
            break;
    }
    
    AVAudioUnitEQFilterParameters *params = [self.eqUnit.bands objectAtIndex:band];
    [self.frequencyKnob refresh];
    [self.frequencyKnob setValue:params.frequency animated:YES];
    [self.gainKnob setValue:params.gain animated:YES];
    [self.bandwidthKnob setValue:params.bandwidth animated:YES];
}

- (IBAction)knobControlValueChanged:(LHKnobControl *)sender
{
    if (sender == self.frequencyKnob)
    {
        [self.eqUnit.bands objectAtIndex:self.band].frequency = sender.value;
        self.freqLabel.text = [NSString stringWithFormat:@"%0.1f Hz", sender.value];
    }
    else if (sender == self.bandwidthKnob)
    {
        [self.eqUnit.bands objectAtIndex:self.band].bandwidth = sender.value;
        self.qLabel.text = [NSString stringWithFormat:@"%0.2f", sender.value];
    }
    else if (sender == self.gainKnob)
    {
        [self.eqUnit.bands objectAtIndex:self.band].gain = sender.value;
        self.gainLabel.text = [NSString stringWithFormat:@"%0.2f dB", sender.value];
    }
}

- (IBAction)bandControlValueChanged:(UISegmentedControl *)sender
{
    self.band = [sender selectedSegmentIndex];
    [self configureKnobsForBand:self.band];
}

@end
