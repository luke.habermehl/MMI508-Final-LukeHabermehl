//
//  AVAudioChain+Dictionary.h
//  LoopMan
//
//  Created by Luke on 4/14/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "AVAudioChain.h"

@interface AVAudioChain (Dictionary)

+ (instancetype)audioChainFromDictionary:(NSDictionary *)dictionary
                              withEngine:(AVAudioEngine *)engine
                               inputNode:(AVAudioNode *)inputNode
                              outputNode:(AVAudioNode *)outputNode;

- (NSDictionary *)dictionary;

@end
