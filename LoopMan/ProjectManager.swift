//
//  ProjectManager.swift
//  LoopMan
//
//  Created by Luke on 4/8/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import Foundation

let kProjectManagerUpdateNotification = "ProjectManagerUpdateNotification"

class ProjectManager {
    private(set) var projects : [Project] = []
    static let sharedInstance = ProjectManager()
    
    init() {
        guard let manifest = NSDictionary(contentsOfFile: PathForProjectManifest()) else {
            return
        }
        
        guard let projectFiles = manifest.objectForKey(kProjectFilesKey) as? NSArray else {
            NSLog("Error: project manifest missing key: \(kProjectFilesKey)")
            return
        }
        
        for fileName in projectFiles {
            let filePath = PathForProjectFileWithFileName(fileName as! String)
            guard let projDict = NSDictionary(contentsOfFile: filePath) else {
                NSLog("Error: could not load project file at path:\(filePath)")
                continue
            }
            
            do {
                let proj = try Project.projectFromDictionary(projDict)
                self.projects.append(proj)
            } catch ProjectLoadError.MissingKey(let name) {
                NSLog("Project Load Error: missing key: \(name)")
            } catch let err as NSError {
                NSLog("Project Load Error: \(err)")
            }
        }
    }
    
    func addProject(project: Project) {
        self.projects.append(project)
        NSNotificationCenter.defaultCenter().postNotificationName(kProjectManagerUpdateNotification, object: nil)
        
        self.updateManifestFile()
    }
    
    func removeProject(project: Project) {
        guard let index = self.projects.indexOf({$0 === project}) else {
            return
        }
        
        project.removeAllLoops()
        self.projects.removeAtIndex(index)
        
        NSNotificationCenter.defaultCenter().postNotificationName(kProjectManagerUpdateNotification, object: nil)
        
        self.updateManifestFile()
        self.removeProjectFiles(project)
    }
    
    private func updateManifestFile() {
        let dict = NSMutableDictionary()
        let projArray = NSMutableArray()
        for p in self.projects {
            projArray.addObject(p.projectFileName() as NSString)
        }
        
        dict.setObject(projArray, forKey: kProjectFilesKey)
        
        dict.writeToFile(PathForProjectManifest(), atomically: true)
    }
    
    private func removeProjectFiles(project: Project) {
        let projectPath = PathForProjectFileWithFileName(project.projectFileName())
        let loopDir = project.pathForLoopDirectory()
        
        do {
            try NSFileManager.defaultManager().removeItemAtPath(projectPath)
            try NSFileManager.defaultManager().removeItemAtPath(loopDir)
        } catch let err as NSError {
            NSLog("File removal error: \(err)")
        }
    }
}

private func PathForProjectManifest() -> String {
    let dirs = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.LibraryDirectory, .UserDomainMask, true) as [String]
    let d = dirs[0] as NSString
    return d.stringByAppendingPathComponent("manifest.plist")
}

private func PathForProjectFileWithFileName(filename: String) -> String {
    let dirs = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)
    let d = dirs[0] as NSString
    return d.stringByAppendingPathComponent(filename)
}

private let kProjectFilesKey = "project_files"
