//
//  LHLabel.h
//  LoopMan
//
//  Created by Luke on 4/16/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LHLabel : UILabel

@property (nonatomic) NSString *defaultText;
@property (nonatomic) NSTimeInterval timeout;

@end
