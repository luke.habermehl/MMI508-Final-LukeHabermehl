//
//  LHLabel.m
//  LoopMan
//
//  Created by Luke on 4/16/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "LHLabel.h"

@interface LHLabel ()

@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSString *temporaryText;

@end

@implementation LHLabel

- (void)onTimerDone
{
    [super setText:self.defaultText];
    self.timer = nil;
}

- (void)setText:(NSString *)text
{
    [self.timer invalidate];
    self.timer = nil;
    [super setText:text];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timeout
                                                  target:self
                                                selector:@selector(onTimerDone)
                                                userInfo:nil
                                                 repeats:NO];
}

- (void)setDefaultText:(NSString *)defaultText
{
    _defaultText = defaultText;
    
    if (!self.timer)
    {
        [super setText:_defaultText];
    }
}

@end
