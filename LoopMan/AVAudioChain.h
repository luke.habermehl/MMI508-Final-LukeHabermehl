//
//  AVAudioChain.h
//  AudioPlayer
//
//  Created by Luke on 3/2/16.
//  Copyright © 2016 University of Miami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class AVAudioChain;

@protocol AVAudioChainDelegate

- (void)audioChainDidStartEditing:(AVAudioChain *)audioChain;
- (void)audioChainDidFinishEditing:(AVAudioChain *)audioChain;

@end

/*! 
    @class AVAudioChain
    @discussion
        The AVAudioChain class provides a simplified API for attaching nodes to an AVAudioEngine instance as well as connecting and reordering them.
 
        Currently, editing the audio chain during playback is not supported.
 */
@interface AVAudioChain : NSObject

@property (nonatomic, weak) AVAudioEngine *engine;
@property (nonatomic, readonly) NSArray *audioUnits;
@property (nonatomic, weak) AVAudioNode *inputNode;
@property (nonatomic, weak) AVAudioNode *outputNode;
@property (nonatomic) NSObject<AVAudioChainDelegate> * delegate;
@property (nonatomic, readonly) NSInteger unitCount;

/*! Initialize an audio chain
 \param engine reference to the AVAudioEngine instance used to attach and connect the nodes
 \param inputNode the node which will be connected to the first item in the chain
 \param outputNode the node which will be connected to from the last item in the chain
 */
- (id)initWithEngine:(nonnull AVAudioEngine *)engine inputNode:(AVAudioNode * _Nonnull)inputNode outputNode:(AVAudioNode * _Nonnull)outputNode;

/*! Append an audio unit to the end of the audio chain (before the output node)
 \param audioUnit the AVAudioUnit to add
 */
- (void)addAudioUnit:( AVAudioUnit * _Nonnull )audioUnit;

/*! Swap two audio units in the chain 
 \param index1 the index of the first object
 \param index2 the index of the object to swap
 */
- (void)swapAudioUnitAtIndex:(NSUInteger)index1 withAudioUnitAtIndex:(NSUInteger)index2;

/*! Move the node at the source index to the destination index by removal and insertion instead of a swap
 \param sourceIndex original index
 \param destinationIndex destination
 */
- (void)moveNodeAtIndex:(NSUInteger)sourceIndex toIndex:(NSUInteger)destinationIndex;

/*! Remove the node at the given index from the audio chain
 */
- (void)removeNodeAtIndex:(NSUInteger)index;

/*! Remove all nodes from the chain. The input and ouput nodes are automatically connected
 */
- (void)removeAllNodes;

@end
