//
//  AVAudioUnitReverb+AVAudioChain.m
//  LoopMan
//
//  Created by Luke on 4/14/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "AVAudioUnitReverb+AVAudioChain.h"
#import <objc/runtime.h>

@implementation AVAudioUnitReverb (AVAudioChain)

static char presetKey;

- (void)setPreset:(AVAudioUnitReverbPreset)preset
{
    objc_setAssociatedObject(self, &presetKey, [NSNumber numberWithInteger:preset], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self loadFactoryPreset:preset];
}

- (AVAudioUnitReverbPreset)preset
{
    NSNumber *number = objc_getAssociatedObject(self, &presetKey);
    if (number == nil)
        return 0;
    
    return (AVAudioUnitReverbPreset)[number integerValue];
}

@end
