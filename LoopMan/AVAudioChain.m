//
//  AVAudioChain.m
//  AudioPlayer
//
//  Created by Luke on 3/2/16.
//  Copyright © 2016 University of Miami. All rights reserved.
//

#import "AVAudioChain.h"

@interface AVAudioChain ()

@property (nonatomic) NSMutableArray *mAudioUnits;

@end

@implementation AVAudioChain

- (id)initWithEngine:(AVAudioEngine *)engine inputNode:(AVAudioNode *)inputNode outputNode:(AVAudioNode *)outputNode
{
    self = [super init];
    if (self)
    {
        self.engine = engine;
        self.inputNode = inputNode;
        self.outputNode = outputNode;
        
        [self.engine connect:self.inputNode to:self.outputNode format:[self.inputNode outputFormatForBus:0]];
        
        _mAudioUnits = [NSMutableArray array];
    }
    
    return self;
}

- (NSArray *)audioUnits
{
    return [NSArray arrayWithArray:self.mAudioUnits];
}

- (NSInteger)unitCount
{
    return [self.mAudioUnits count];
}

#pragma mark - API

- (void)addAudioUnit:(AVAudioUnit *)audioUnit
{
    [self.delegate audioChainDidStartEditing:self];
    
    [self.engine attachNode:audioUnit];
    if (self.mAudioUnits.count > 0)
    {
        [self.engine disconnectNodeInput:self.outputNode];
        [self.engine disconnectNodeOutput:self.mAudioUnits.lastObject];
        [self.engine connect:self.mAudioUnits.lastObject to:audioUnit format:[self.mAudioUnits.lastObject outputFormatForBus:0]];
    }
    else
    {
        [self.engine disconnectNodeOutput:self.inputNode];
        [self.engine connect:self.inputNode to:audioUnit format:[self.inputNode outputFormatForBus:0]];
    }
    
    [self.engine connect:audioUnit to:self.outputNode format:[audioUnit outputFormatForBus:0]];
    
    [self.mAudioUnits addObject:audioUnit];
    
    [self.delegate audioChainDidFinishEditing:self];
}

- (void)swapAudioUnitAtIndex:(NSUInteger)index1 withAudioUnitAtIndex:(NSUInteger)index2
{
    NSParameterAssert(index1 < self.mAudioUnits.count);
    NSParameterAssert(index2 < self.mAudioUnits.count);
    
    AVAudioUnit *unit1 = [self.mAudioUnits objectAtIndex:index1];
    AVAudioUnit *unit2 = [self.mAudioUnits objectAtIndex:index2];
    
    [self disconnectAll];
    
    //Swap
    [self.mAudioUnits replaceObjectAtIndex:index1 withObject:unit2];
    [self.mAudioUnits replaceObjectAtIndex:index2 withObject:unit1];
    
    [self connectAll];
    
}

- (void)moveNodeAtIndex:(NSUInteger)sourceIndex toIndex:(NSUInteger)destinationIndex
{
    NSParameterAssert(sourceIndex < self.mAudioUnits.count);
    NSParameterAssert(destinationIndex < self.mAudioUnits.count);
    
    [self disconnectAll];
    
    AVAudioUnit *unit = [self.mAudioUnits objectAtIndex:sourceIndex];
    [self.mAudioUnits removeObjectAtIndex:sourceIndex];
    [self.mAudioUnits insertObject:unit atIndex:destinationIndex];
    
    [self connectAll];
}

- (void)removeNodeAtIndex:(NSUInteger)index
{
    NSParameterAssert(index < self.mAudioUnits.count);
    
    [self disconnectAll];
    [self.mAudioUnits removeObjectAtIndex:index];
    
    [self connectAll];
}

- (void)removeAllNodes
{
    [self disconnectAll];
    [self.mAudioUnits removeAllObjects];
    [self connectAll];
}

- (void)disconnectAll
{
    [self.delegate audioChainDidStartEditing:self];
    
    //Disconnect input/output
    [self.engine disconnectNodeOutput:self.inputNode];
    [self.engine disconnectNodeInput:self.outputNode];
    
    //Disconnect all nodes in chain
    for (AVAudioUnit *unit in self.mAudioUnits)
    {
        [self.engine disconnectNodeInput:unit];
        [self.engine disconnectNodeOutput:unit];
    }
}

#pragma mark - Private

- (void)connectAll
{
    if (self.mAudioUnits.count == 0)
    {
        [self.engine connect:self.inputNode to:self.outputNode format:[self.inputNode outputFormatForBus:0]];
        [self.delegate audioChainDidFinishEditing:self];
        return;
    }
    
    //Reconnect chain in order
    AVAudioNode *prev = self.inputNode;
    for (AVAudioUnit *unit in self.mAudioUnits)
    {
        [self.engine connect:prev to:unit format:[prev outputFormatForBus:0]];
        prev = unit;
    }
    
    [self.engine connect:self.mAudioUnits.lastObject to:self.outputNode format:[self.mAudioUnits.lastObject outputFormatForBus:0]];
    
    [self.delegate audioChainDidFinishEditing:self];

}

@end
