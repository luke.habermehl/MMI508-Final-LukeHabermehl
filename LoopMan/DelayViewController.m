//
//  DelayViewController.m
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "DelayViewController.h"
#import "LHKnobControl.h"
#import "LHLabel.h"

@interface DelayViewController ()

@property (weak, nonatomic) IBOutlet UISlider *wetDrySlider;
@property (weak, nonatomic) IBOutlet LHLabel *wetDryLabel;

@property (weak, nonatomic) IBOutlet LHKnobControl *delayTimeKnob;
@property (weak, nonatomic) IBOutlet LHKnobControl *feedbackKnob;

@property (weak, nonatomic) IBOutlet LHLabel *timeLabel;
@property (weak, nonatomic) IBOutlet LHLabel *feedbackLabel;


@end

@implementation DelayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"delay", nil);
    
    for (LHKnobControl *knob in @[self.delayTimeKnob, self.feedbackKnob])
    {
        [knob setBackgroundColor:[UIColor clearColor]];
        [knob addTarget:self
                 action:@selector(knobControlValueChanged:)
       forControlEvents:UIControlEventValueChanged];
        
        [knob setImage:[UIImage imageNamed:@"knob2.png"]];
        
        knob.imageHeight = 64;
        knob.imageWidth = 64;
        knob.frameCount = 31;
    }

    self.wetDrySlider.value = self.delayUnit.wetDryMix;
    self.delayTimeKnob.minimumValue = 0.0;
    self.delayTimeKnob.maximumValue = 1.0;
    [self.delayTimeKnob setValue:self.delayUnit.delayTime];
    
    self.feedbackKnob.minimumValue = 0;
    self.feedbackKnob.maximumValue = 100;
    [self.feedbackKnob setValue:self.delayUnit.feedback];
    
    self.timeLabel.defaultText = NSLocalizedString(@"time", nil);
    self.timeLabel.timeout = 1.0;
    self.feedbackLabel.defaultText = NSLocalizedString(@"feedback", nil);
    self.feedbackLabel.timeout = 1.0;
    self.wetDryLabel.defaultText = NSLocalizedString(@"mix", nil);
    self.wetDryLabel.timeout = 1.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)wetDrySliderValueChanged:(id)sender
{
    [self.delayUnit setWetDryMix:self.wetDrySlider.value];
    [self.wetDryLabel setText:[NSString stringWithFormat:@"%0.0f %%", self.wetDrySlider.value]];
}

- (IBAction)knobControlValueChanged:(LHKnobControl *)sender
{
    if (sender == self.delayTimeKnob)
    {
        [self.delayUnit setDelayTime:sender.value];
        self.timeLabel.text = [NSString stringWithFormat:@"%0.2f msec", sender.value];
    }
    else if (sender == self.feedbackKnob)
    {
        [self.delayUnit setFeedback:sender.value];
        self.feedbackLabel.text = [NSString stringWithFormat:@"%.0f %%", sender.value];
    }
}


@end
