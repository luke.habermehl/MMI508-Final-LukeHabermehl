//
//  RecordLoopTableViewCell.swift
//  LoopMan
//
//  Created by Luke on 4/10/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import UIKit

class RecordLoopTableViewCell: UITableViewCell {

    @IBOutlet weak var recordButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
