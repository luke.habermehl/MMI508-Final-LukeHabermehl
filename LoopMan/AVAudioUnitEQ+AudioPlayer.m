//
//  AVAudioUnitEQ+AudioPlayer.m
//  AudioPlayer
//
//  Created by Luke on 3/2/16.
//  Copyright © 2016 University of Miami. All rights reserved.
//

#import "AVAudioUnitEQ+AudioPlayer.h"

@implementation AVAudioUnitEQ (AudioPlayer)

+ (AVAudioUnitEQ *)defaultFourBandEqualizer
{
    AVAudioUnitEQ *eq = [[AVAudioUnitEQ alloc] initWithNumberOfBands:4];
    
    AVAudioUnitEQFilterParameters *lowParams = [eq.bands objectAtIndex:0];
    lowParams.filterType = AVAudioUnitEQFilterTypeLowShelf;
    lowParams.gain = 0.0;
    lowParams.frequency = 40;
    lowParams.bypass = NO;
    
    AVAudioUnitEQFilterParameters *lowMidParams = [eq.bands objectAtIndex:1];
    lowMidParams.filterType = AVAudioUnitEQFilterTypeParametric;
    lowMidParams.frequency = 500;
    lowMidParams.gain = 0.0;
    lowMidParams.bypass = NO;
    
    AVAudioUnitEQFilterParameters *highMidParams = [eq.bands objectAtIndex:2];
    highMidParams.filterType = AVAudioUnitEQFilterTypeParametric;
    highMidParams.frequency = 2000;
    highMidParams.gain = 0.0;
    highMidParams.bypass = NO;
    
    AVAudioUnitEQFilterParameters *highParams = [eq.bands objectAtIndex:3];
    highParams.filterType = AVAudioUnitEQFilterTypeHighShelf;
    highParams.frequency = 8000;
    highParams.gain = 0.0;
    highParams.bypass = NO;
    
    eq.bypass = NO;
    
    return eq;
}

@end
