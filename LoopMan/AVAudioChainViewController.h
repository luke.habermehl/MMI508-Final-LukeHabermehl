//
//  AVAudioChainViewController.h
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVAudioChain.h"

@interface AVAudioChainViewController : UIViewController

@property (weak, nonatomic) AVAudioChain *audioChain;

@end
