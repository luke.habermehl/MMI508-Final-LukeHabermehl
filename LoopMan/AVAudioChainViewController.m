//
//  AVAudioChainViewController.m
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "AVAudioChainViewController.h"
#import "EQViewController.h"
#import "ReverbViewController.h"
#import "DelayViewController.h"
#import "NewEffectViewController.h"

@interface AVAudioChainViewController () <UITableViewDataSource, UITableViewDelegate, NewEffectViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *emptyTableLabel;

@end

@implementation AVAudioChainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSParameterAssert(self.audioChain);
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = @"Audio Chain";
    [self.emptyTableLabel setText:NSLocalizedString(@"tap edit to add effect", nil)];
    
    [self reloadTableView];
    [self setupBarButtons];
}

- (void)addButtonPressed
{
    NewEffectViewController *vc = [[UIStoryboard storyboardWithName:@"AVAudioChain" bundle:nil] instantiateViewControllerWithIdentifier:@"new_effect_view_controller"];
    
    vc.delegate = self;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)editButtonPressed
{
    [self.tableView setEditing:!self.tableView.isEditing];
    
    [self setupBarButtonsForEditing:self.tableView.isEditing];
    
    if ([self.tableView isEditing])
    {
        [self.emptyTableLabel setText:NSLocalizedString(@"tap add to add effect", nil)];
    }
    
    else
    {
        [self.emptyTableLabel setText:NSLocalizedString(@"tap edit to add effect", nil)];
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.audioChain.audioUnits.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"audio_chain_cell"];
    cell.textLabel.text = [self stringForAudioUnit:[self.audioChain.audioUnits objectAtIndex:indexPath.row]];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AVAudioUnit *audioUnit = [self.audioChain.audioUnits objectAtIndex:indexPath.row];
    
    if ([audioUnit isKindOfClass:[AVAudioUnitEQ class]])
    {
        EQViewController *vc = [[UIStoryboard storyboardWithName:@"AVAudioChain" bundle:nil] instantiateViewControllerWithIdentifier:@"eq_view_controller"];
        
        vc.eqUnit = (AVAudioUnitEQ *)audioUnit;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    else if ([audioUnit isKindOfClass:[AVAudioUnitReverb class]])
    {
        ReverbViewController *vc = [[UIStoryboard storyboardWithName:@"AVAudioChain" bundle:nil] instantiateViewControllerWithIdentifier:@"reverb_view_controller"];
        
        vc.reverbUnit = (AVAudioUnitReverb *)audioUnit;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    else if ([audioUnit isKindOfClass:[AVAudioUnitDelay class]])
    {
        DelayViewController *vc = [[UIStoryboard storyboardWithName:@"AVAudioChain" bundle:nil] instantiateViewControllerWithIdentifier:@"delay_view_controller"];
        
        vc.delayUnit = (AVAudioUnitDelay *)audioUnit;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [self.audioChain moveNodeAtIndex:sourceIndexPath.row toIndex:destinationIndexPath.row];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self.audioChain removeNodeAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        [self reloadTableView];
    }
}

#pragma mark - NewEffectViewControllerDelegate

- (void)didCreateNewEffect:(AVAudioUnit *)audioUnit
{
    [self.audioChain addAudioUnit:audioUnit];
    [self reloadTableView];
    [self.tableView setEditing:NO];
    [self setupBarButtonsForEditing:NO];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didCancelCreateNewEffect
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private

- (void)reloadTableView
{
    [self.tableView reloadData];
    
    if (self.audioChain.unitCount == 0)
    {
        [self.tableView setHidden:YES];
        [self.emptyTableLabel setHidden:NO];
    }
    else
    {
        [self.tableView setHidden:NO];
        [self.emptyTableLabel setHidden:YES];
    }
}

- (void)setupBarButtons
{
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(editButtonPressed)];
    
    self.navigationItem.rightBarButtonItem = editButton;
}

- (void)setupBarButtonsForEditing:(BOOL)editing
{
    if (editing)
    {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Add"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(addButtonPressed)];
        
        self.navigationItem.leftBarButtonItem = addButton;
        self.navigationItem.rightBarButtonItem.title = @"Done";
    }
    
    else
    {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem.title = @"Edit";
    }
}

- (NSString *)stringForAudioUnit:(AVAudioUnit *)audioUnit
{
    if ([audioUnit isKindOfClass:[AVAudioUnitReverb class]])
    {
        return NSLocalizedString(@"reverb", nil);
    }
    else if ([audioUnit isKindOfClass:[AVAudioUnitEQ class]])
    {
        return NSLocalizedString(@"equalizer", nil);
    }
    else if ([audioUnit isKindOfClass:[AVAudioUnitDelay class]])
    {
        return NSLocalizedString(@"delay", nil);
    }
    
    return NSLocalizedString(@"effect", nil);
}



@end
