//
//  Loop.swift
//  LoopMan
//
//  Created by Luke on 4/8/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import AVFoundation

protocol LoopDelegate {
    func loopDidStopPlaying(loop: Loop)
    func loopDidStartPlaying(loop: Loop)
    func loopDidStartRecording(loop: Loop)
    func loopDidStopRecording(loop: Loop)
}

class Loop : NSObject, AVAudioChainDelegate {
    var name : String
    var delegate : LoopDelegate?
    var audioChain : AVAudioChain
    
    private(set) var recording : Bool = false
    private(set) var playing : Bool = false
    
    private let engine = AVAudioEngine()
    private var file : AVAudioFile?
    private var playerNode : AVAudioPlayerNode?
    private unowned var project : Project
    private var readyToPlay : Bool = false
    private var eqNode : AVAudioUnitEQ
    
    init(name: String, project: Project, file: AVAudioFile?) {
        self.name = name
        self.project = project
        self.file = file
        
        let eq = AVAudioUnitEQ(numberOfBands: 1)
        
        eq.bands[0].bandwidth = 2.0
        eq.bands[0].frequency = 80000
        eq.bands[0].filterType = .LowPass
        
        self.eqNode = eq
        self.engine.attachNode(eq)
        
        self.audioChain = AVAudioChain(engine: self.engine, inputNode: self.eqNode, outputNode: self.engine.mainMixerNode)
        
        super.init()
        
        self.audioChain.delegate = self
    }
    
    class func loopWithFile(fileURL: NSURL, project: Project) -> Loop? {
        do {
            let audioFile = try AVAudioFile(forReading: fileURL)
            let name = (fileURL.pathComponents!.last! as NSString).stringByDeletingPathExtension
            
            let loop = Loop(name: name, project: project, file: audioFile)
            loop.readyToPlay = true
            
            return loop
        } catch let err as NSError {
            NSLog("Error: \(err)")
        }
        
        return nil
    }
    
    class func loopFromDictionary(dictionary: NSDictionary, project: Project) -> Loop? {
        let filepath = dictionary.objectForKey(kFileNameKey) as! String
        let audioChainDict = dictionary.objectForKey(kAudioChainKey) as! NSDictionary
        
        guard let loop = Loop.loopWithFile(Loop.urlForLoopWithFileName(filepath, inProject: project), project: project) else {
            return nil
        }
        
        loop.audioChain = AVAudioChain(fromDictionary: audioChainDict as! [NSObject : AnyObject], withEngine: loop.engine, inputNode: loop.eqNode, outputNode: loop.engine.mainMixerNode)
        
        return loop
    }
    
    func record() {
        if self.recording {
            return
        }
        
        self.recording = true
        self.readyToPlay = false
        do {
            self.file = try AVAudioFile(forWriting: Loop.urlForLoopWithFileName(self.fileName(), inProject: self.project), settings: (self.engine.inputNode?.outputFormatForBus(0).settings)!)
            self.engine.inputNode?.installTapOnBus(0, bufferSize: 1024, format: self.engine.inputNode?.outputFormatForBus(0), block: { (buffer, time) in
                do {
                    try self.file!.writeFromBuffer(buffer)
                } catch {
                    //TODO: handle error?
                }
            })
            
            try self.engine.start()
            self.delegate?.loopDidStartRecording(self)
            
        } catch let err as NSError {
            NSLog("Error: \(err)")
            self.recording = false
        }
    }
    
    func stop() {
        if self.recording {
            self.engine.inputNode?.removeTapOnBus(0)
            self.recording = false
            self.delegate?.loopDidStopRecording(self)
            self.engine.stop()
            self.playAtTime(nil)
            return
        }
        
        if self.playing {
            self.playerNode!.stop()
            self.playing = false
            self.delegate?.loopDidStopPlaying(self)
        }
        
        if engine.running {
            engine.stop()
        }
        
        if self.playerNode != nil {
            self.engine.disconnectNodeOutput(self.playerNode!)
            self.playerNode = nil
        }
        
        self.prepareForPlay()
    }
    
    func fileName() -> String {
        return "\(self.name.slug).caf"
    }
    
    func playAtTime(time: AVAudioTime?) {
        if self.recording || self.playing {
            return
        }
        
        self.playing = true
        let player = AVAudioPlayerNode()
        self.engine.attachNode(player)
        self.engine.connect(player, to: self.eqNode, format: nil)
        do {
            if (!self.readyToPlay) {
                self.prepareForPlay()
            }
            let buffer = AVAudioPCMBuffer(PCMFormat: self.file!.processingFormat, frameCapacity: UInt32(self.file!.length))
            try self.file!.readIntoBuffer(buffer)
            try engine.start()
            self.playerNode = player
            player.play()
            player.scheduleBuffer(buffer, atTime: time, options: .Loops, completionHandler: nil)
            self.delegate?.loopDidStartPlaying(self)
        } catch let err as NSError {
            NSLog("Error: \(err)")
            self.playing = false
        }
    }
    
    func prepareForPlay() {
        do {
            self.file = try AVAudioFile(forReading: Loop.urlForLoopWithFileName(self.fileName(), inProject: self.project))
            self.readyToPlay = true
        } catch let err as NSError {
            NSLog("prepareForPlay error: \(err)")
        }
    }
    
    func toDict() -> NSDictionary {
        let dict = NSMutableDictionary()
        dict.setObject(self.fileName(), forKey: kFileNameKey)
        dict.setObject(self.audioChain.dictionary(), forKey: kAudioChainKey)
        
        return dict
    }
    
    static func urlForLoopWithFileName(filename: String, inProject project: Project) -> NSURL {
        let loopsDir = project.pathForLoopDirectory() as NSString
        let path = loopsDir.stringByAppendingPathComponent(filename)
        
        return NSURL(fileURLWithPath: path)
    }
    
    //MARK: AVAudioChainDelegate
    func audioChainDidStartEditing(audioChain: AVAudioChain!) {
        NSLog("audioChainDidStartEditing")
        self.stop()
    }
    
    func audioChainDidFinishEditing(audioChain: AVAudioChain!) {
        self.prepareForPlay()
    }
}

private let kFileNameKey = "filename"
private let kAudioChainKey = "audio_chain"
