//
//  AVAudioUnitReverb+AVAudioChain.h
//  LoopMan
//
//  Created by Luke on 4/14/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVAudioUnitReverb (AVAudioChain)

@property (nonatomic) AVAudioUnitReverbPreset preset;

@end
