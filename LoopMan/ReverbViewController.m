//
//  ReverbViewController.m
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "ReverbViewController.h"
#import "AVAudioUnitReverb+AVAudioChain.h"
#import "LHLabel.h"

@interface ReverbViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *presetPicker;
@property (weak, nonatomic) IBOutlet UISlider *wetDrySlider;
@property (weak, nonatomic) IBOutlet LHLabel *wetDryLabel;


@end

@implementation ReverbViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"reverb", nil);
    self.wetDrySlider.value = self.reverbUnit.wetDryMix;
    self.presetPicker.delegate = self;
    self.presetPicker.dataSource = self;
    
    [self.presetPicker reloadAllComponents];
    [self.presetPicker selectRow:self.reverbUnit.preset inComponent:0 animated:NO];
    
    self.wetDryLabel.defaultText = NSLocalizedString(@"mix", nil);
    self.wetDryLabel.timeout = 1.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)wetDrySliderValueChanged:(id)sender
{
    self.reverbUnit.wetDryMix = self.wetDrySlider.value;
    [self.wetDryLabel setText:[NSString stringWithFormat:@"%0.0f %%", self.wetDrySlider.value]];
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 13;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[ReverbViewController presetNames] objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self.reverbUnit setPreset:row];
}

#pragma mark - Private

+ (NSArray *)presetNames
{
    static NSArray *_presetNames;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _presetNames = @[
                         @"Small Room",
                         @"Medium Room",
                         @"Large Room",
                         @"Medium Hall",
                         @"Large Hall",
                         @"Plate",
                         @"Medium Chamber",
                         @"Large Chamber",
                         @"Cathedral",
                         @"Large Room 2",
                         @"Medium Hall 2",
                         @"Medium Hall 3",
                         @"Large Hall 2"
                         ];

    });
    
    return _presetNames;
}

@end
