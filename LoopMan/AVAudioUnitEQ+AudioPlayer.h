//
//  AVAudioUnitEQ+AudioPlayer.h
//  AudioPlayer
//
//  Created by Luke on 3/2/16.
//  Copyright © 2016 University of Miami. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVAudioUnitEQ (AudioPlayer)

/*! A four band parametric EQ with bands centered at 40 Hz, 500 Hz, 2 kHz, and 8 kHz. The low and high bands are by default, low shelf and high shelf filters, respectively.
 */
+ (AVAudioUnitEQ *)defaultFourBandEqualizer;

@end
