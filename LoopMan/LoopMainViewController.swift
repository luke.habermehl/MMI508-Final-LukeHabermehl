//
//  LoopMainViewController.swift
//  LoopMan
//
//  Created by Luke on 4/8/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import UIKit

class LoopMainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, LoopTableViewCellDelegate {
    
    weak var project : Project?
    var loops : [Loop] = []
    var recordingLoop : Loop?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(self.project != nil)
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.title = self.project!.name
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.hidden = true
        
        self.setupBarButtons()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onProjectUpdate), name: kProjectUpdateNotification, object: self.project!)
        
        activitySpinner.startAnimating()
        
        self.project?.loadLoops(completion: {
            self.loops = self.project!.loops
            self.tableView.reloadData()
            self.activitySpinner.stopAnimating()
            self.activitySpinner.hidden = true
            self.tableView.hidden = false
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = self.project!.name
        for cell in self.tableView.visibleCells {
            if let c = cell as? LoopTableViewCell {
                c.configurePlayingIndicator()
            }
        }
    }
    
    //MARK: Actions
    func onProjectUpdate(notification: NSNotification) {
        if notification.object! === self.project!  {
            self.loops = self.project!.loops
            self.tableView.reloadData()
        }
    }
    
    func recordButtonTouchDown(sender: AnyObject) {
        self.recordingLoop = Loop(name: "loop \(self.loops.count+1)", project: self.project!, file: nil)
        self.recordingLoop!.record()
    }
    
    func recordButtonTouchUp(sender: AnyObject) {
        self.recordingLoop?.stop()
        self.project!.addLoop(self.recordingLoop!)
        self.recordingLoop = nil
    }
    
    func editButtonPressed(sender: UIBarButtonItem) {
        if !self.tableView.editing {
            self.tableView.editing = true
            self.navigationItem.rightBarButtonItem!.title = "Done"
        }
        
        else {
            self.tableView.editing = false
            self.navigationItem.rightBarButtonItem!.title = "Edit"
        }
    }
    
    func closeButtonPressed() {
        self.project!.unloadLoops()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: LoopTableViewCellDelegate {
    func presentEffectsForLoop(loop: Loop) {
        let vc = UIStoryboard(name: "AVAudioChain", bundle: nil).instantiateInitialViewController() as! AVAudioChainViewController
        vc.audioChain = loop.audioChain
        self.navigationItem.title = "Back"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.loops.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.loops.count == 0 || indexPath.row == self.loops.count {
            let cell = self.tableView.dequeueReusableCellWithIdentifier("record_loop_cell") as! RecordLoopTableViewCell
            cell.recordButton.addTarget(self, action: #selector(recordButtonTouchDown), forControlEvents: UIControlEvents.TouchDown)
            cell.recordButton.addTarget(self, action: #selector(recordButtonTouchUp), forControlEvents: .TouchUpInside)
            cell.recordButton.addTarget(self, action: #selector(recordButtonTouchUp), forControlEvents: .TouchUpOutside)
            
            return cell
        }

        else {
            let cell = self.tableView.dequeueReusableCellWithIdentifier("loop_cell") as! LoopTableViewCell
            let loop = self.loops[indexPath.row]
            
            cell.nameLabel!.text = loop.name
            cell.loop = loop
            cell.delegate = self
            loop.delegate = cell
            if loop.playing {
                cell.state = .Playing
            }
            
            return cell
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //MARK: UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < self.loops.count {
            let loop = self.loops[indexPath.row]
            if loop.playing {
                loop.stop()
            }
                
            else {
                loop.playAtTime(nil)
            }
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if self.loops.count == 0 || indexPath.row == self.loops.count {
            return false
        }
        
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let loop = self.loops[indexPath.row]
            self.project!.removeLoop(loop)
        }
    }
    
    private func setupBarButtons() {
        let editButton = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action: #selector(editButtonPressed))
        self.navigationItem.rightBarButtonItem = editButton
        
        let closeButton = UIBarButtonItem(title: "Close", style: .Plain, target: self, action: #selector(closeButtonPressed))
        self.navigationItem.leftBarButtonItem = closeButton
    }
}
