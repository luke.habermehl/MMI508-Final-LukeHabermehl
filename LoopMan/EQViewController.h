//
//  EQViewController.h
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface EQViewController : UIViewController

@property (weak, nonatomic) AVAudioUnitEQ *eqUnit;

@end
