//
//  ProjectListViewController.swift
//  LoopMan
//
//  Created by Luke on 4/8/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import UIKit

class ProjectListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var projects : [Project] = ProjectManager.sharedInstance.projects
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.title = "Projects"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onProjectManagerUpdate), name: kProjectManagerUpdateNotification, object: nil)
        
        self.setupBarButtons()
        
        if self.projects.count == 0 {
            self.tableView.hidden = true
        }
        
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func editButtonPressed(sender: UIBarButtonItem) {
        if !self.tableView.editing {
            self.tableView.editing = true
            self.navigationItem.leftBarButtonItem!.title = "Done"
        }
            
        else {
            self.tableView.editing = false
            self.navigationItem.leftBarButtonItem!.title = "Edit"
        }
    }
    
    func newProjectButtonPressed() {
        let alertVC = UIAlertController(title: "New Project", message: "Give your new project a name", preferredStyle: UIAlertControllerStyle.Alert)
        alertVC.addTextFieldWithConfigurationHandler { (textField : UITextField) in
            textField.placeholder = "Project Name"
        }
        
        unowned let unownedSelf = self
        alertVC.addAction(UIAlertAction(title: "Submit", style: .Default, handler: { (action : UIAlertAction) in
            do {
                let name = alertVC.textFields!.first!.text
                let proj = try Project.projectWithName(name!)
                ProjectManager.sharedInstance.addProject(proj)
            } catch let err as NSError {
                NSLog("Error: could not create new project: \(err)")
                let errorAlert = UIAlertController(title: "Error", message: "Something went wrong and your project couldn't be created", preferredStyle: .Alert)
                errorAlert.addAction(UIAlertAction(title: "Dismiss", style: .Cancel, handler: nil))
                dispatch_async(dispatch_get_main_queue(), { 
                    unownedSelf.presentViewController(errorAlert, animated: true, completion: nil)
                })
            }
        }))
        
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        self.presentViewController(alertVC, animated: true, completion: nil)
    }
    
    func onProjectManagerUpdate() {
        self.projects = ProjectManager.sharedInstance.projects
        self.tableView.hidden = self.projects.count == 0
        self.tableView.reloadData()
    }
    
    //MARK: UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.projects.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("project_list_cell")!
        cell.textLabel?.text = self.projects[indexPath.row].name
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //MARK: UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("loop_vc") as! LoopMainViewController
        
        vc.project = self.projects[indexPath.row]
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let confirmSheet = UIAlertController(title: "Confirm Project Deletion", message: "Are you sure you want to delete this project?", preferredStyle: .ActionSheet)
        
        confirmSheet.addAction(UIAlertAction(title: "Delete", style: .Destructive, handler: { (action) in
            ProjectManager.sharedInstance.removeProject(self.projects[indexPath.row])
        }))
        
        confirmSheet.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        self.presentViewController(confirmSheet, animated: true, completion: nil)
    }
    
    private func setupBarButtons() {
        let newProjectButton = UIBarButtonItem(title: "New", style: .Plain, target: self, action: #selector(newProjectButtonPressed))
        
        self.navigationItem.rightBarButtonItem = newProjectButton
        
        let editButton = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action: #selector(editButtonPressed))
        
        self.navigationItem.leftBarButtonItem = editButton
    }
}
