//
//  LoopTableViewCell.swift
//  LoopMan
//
//  Created by Luke on 4/10/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import UIKit

enum LoopTableViewCellState {
    case Playing
    case Stopped
}

protocol LoopTableViewCellDelegate {
    func presentEffectsForLoop(loop: Loop)
}

class LoopTableViewCell: UITableViewCell, LoopDelegate {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var fxButton: UIButton!
    
    weak var loop : Loop?
    var delegate : LoopTableViewCellDelegate?
    
    var state : LoopTableViewCellState = .Stopped {
        didSet {
            self.configurePlayingIndicator()
        }
    }
    
    func configurePlayingIndicator() {
        if self.state == .Stopped {
            self.speakerImageView.hidden = true
        }
        else {
            self.speakerImageView.hidden = false
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configurePlayingIndicator()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.loop?.delegate = nil
    }
    
    @IBAction func fxButtonPressed(sender: AnyObject) {
        self.delegate!.presentEffectsForLoop(self.loop!)
    }
    
    //MARK: LoopDelegate
    func loopDidStartPlaying(loop: Loop) {
        self.state = .Playing
    }
    
    func loopDidStopPlaying(loop: Loop) {
        self.state = .Stopped
    }
    
    func loopDidStartRecording(loop: Loop) {
        return
    }
    
    func loopDidStopRecording(loop: Loop) {
        return
    }
}
