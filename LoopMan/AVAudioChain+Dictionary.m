//
//  AVAudioChain+Dictionary.m
//  LoopMan
//
//  Created by Luke on 4/14/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "AVAudioChain+Dictionary.h"
#import "AVAudioUnitReverb+AVAudioChain.h"
#import "AVAudioUnitEQ+AudioPlayer.h"

static const NSString *kUnitTypeKey = @"unit_type";
static const NSString *kUnitParametersKey = @"parameters";

@implementation AVAudioChain (Dictionary)

- (NSDictionary *)dictionary
{
    NSMutableArray *unitDictArray = [NSMutableArray arrayWithCapacity:self.audioUnits.count];
    
    for (AVAudioUnit *unit in self.audioUnits)
    {
        NSMutableDictionary *unitDict = [NSMutableDictionary new];
        
        if ([unit isKindOfClass:[AVAudioUnitEQ class]])
        {
            AVAudioUnitEQ *eq = (AVAudioUnitEQ *)unit;
            [unitDict setObject:@"eq" forKey:kUnitTypeKey];
            NSMutableDictionary *params = [NSMutableDictionary new];
            for (NSUInteger bandNo=0; bandNo < 4; bandNo++)
            {
                AVAudioUnitEQFilterParameters *filterParams = [eq.bands objectAtIndex:bandNo];
                [params setObject:[NSNumber numberWithFloat:filterParams.frequency]
                           forKey:[NSString stringWithFormat:@"freq%lu", bandNo]];
                
                [params setObject:[NSNumber numberWithFloat:filterParams.bandwidth]
                           forKey:[NSString stringWithFormat:@"bandwidth%lu", bandNo]];
                
                [params setObject:[NSNumber numberWithFloat:filterParams.gain]
                           forKey:[NSString stringWithFormat:@"gain%lu", bandNo]];
            }
            
            [unitDict setObject:params forKey:kUnitParametersKey];
            [unitDictArray addObject:unitDict];
        }
        
        else if ([unit isKindOfClass:[AVAudioUnitDelay class]])
        {
            AVAudioUnitDelay *delay = (AVAudioUnitDelay *)unit;
            [unitDict setObject:@"delay" forKey:kUnitTypeKey];
            NSMutableDictionary *params = [NSMutableDictionary new];
            
            [params setObject:[NSNumber numberWithFloat:delay.delayTime] forKey:@"delay_time"];
            [params setObject:[NSNumber numberWithFloat:delay.feedback] forKey:@"feedback"];
            [params setObject:[NSNumber numberWithFloat:delay.wetDryMix] forKey:@"mix"];
            
            [unitDict setObject:params forKey:kUnitParametersKey];
            [unitDictArray addObject:unitDict];
        }
        
        else if ([unit isKindOfClass:[AVAudioUnitReverb class]])
        {
            AVAudioUnitReverb *reverb = (AVAudioUnitReverb *)unit;
            [unitDict setObject:@"reverb" forKey:kUnitTypeKey];
            NSMutableDictionary *params = [NSMutableDictionary new];
            
            [params setObject:[NSNumber numberWithInteger:reverb.preset] forKey:@"preset"];
            [params setObject:[NSNumber numberWithFloat:reverb.wetDryMix] forKey:@"mix"];
            
            [unitDict setObject:params forKey:kUnitParametersKey];
            [unitDictArray addObject:unitDict];
        }
    }
    
    return @{@"audio_units":unitDictArray};
}

+ (instancetype)audioChainFromDictionary:(NSDictionary *)dictionary
                                withEngine:(AVAudioEngine *)engine
                               inputNode:(AVAudioNode *)inputNode
                              outputNode:(AVAudioNode *)outputNode
{
    AVAudioChain *audioChain = [[AVAudioChain alloc] initWithEngine:engine inputNode:inputNode outputNode:outputNode];
    NSArray *unitDictArray = [dictionary objectForKey:@"audio_units"];
    
    for (NSDictionary *unitDict in unitDictArray)
    {
        NSString *typeStr = [unitDict objectForKey:kUnitTypeKey];
        if ([typeStr isEqualToString:@"eq"])
        {
            AVAudioUnitEQ *eq = [AVAudioUnitEQ defaultFourBandEqualizer];
            NSDictionary *params = [unitDict objectForKey:kUnitParametersKey];
            for (NSUInteger bandNo = 0; bandNo < 4; bandNo++)
            {
                float frequency = [((NSNumber *)[params objectForKey:[NSString stringWithFormat:@"freq%lu", bandNo]]) floatValue];
                float bandwidth = [((NSNumber *)[params objectForKey:[NSString stringWithFormat:@"bandwidth%lu", bandNo]]) floatValue];
                float gain = [((NSNumber *)[params objectForKey:[NSString stringWithFormat:@"gain%lu", bandNo]]) floatValue];
                
                [eq.bands objectAtIndex:bandNo].frequency = frequency;
                [eq.bands objectAtIndex:bandNo].bandwidth = bandwidth;
                [eq.bands objectAtIndex:bandNo].gain = gain;
            }
            
            [audioChain addAudioUnit:eq];
        }
        
        else if ([typeStr isEqualToString:@"delay"])
        {
            AVAudioUnitDelay *delay = [[AVAudioUnitDelay alloc] init];
            NSDictionary *params = [unitDict objectForKey:kUnitParametersKey];
            
            NSNumber *delayTimeNum = [params objectForKey:@"delay_time"];
            NSNumber *feedbackNum = [params objectForKey:@"feedback"];
            NSNumber *mixNum = [params objectForKey:@"mix"];
            
            delay.delayTime = [delayTimeNum floatValue];
            delay.feedback = [feedbackNum floatValue];
            delay.wetDryMix = [mixNum floatValue];
            
            [audioChain addAudioUnit:delay];
        }
        
        else if ([typeStr isEqualToString:@"reverb"])
        {
            AVAudioUnitReverb *reverb = [[AVAudioUnitReverb alloc] init];
            NSDictionary *params = [unitDict objectForKey:kUnitParametersKey];
            
            NSNumber *presetNum = [params objectForKey:@"preset"];
            NSNumber *mixNum = [params objectForKey:@"mix"];
            
            reverb.preset = [presetNum integerValue];
            reverb.wetDryMix = [mixNum floatValue];
            
            [audioChain addAudioUnit:reverb];
        }
    }
    
    return audioChain;
}

@end
