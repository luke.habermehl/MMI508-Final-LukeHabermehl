//
//  String+LoopMan.swift
//  LoopMan
//
//  Created by Luke on 4/9/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import Foundation

extension String {
    var slug : String {
        get {
            return self.stringByReplacingOccurrencesOfString(" ", withString: "_").lowercaseString
        }
    }
}