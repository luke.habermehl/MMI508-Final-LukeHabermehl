//
//  Project.swift
//  LoopMan
//
//  Created by Luke on 4/8/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

import AVFoundation

let kProjectUpdateNotification = "ProjectUpdateNotification"

class Project {
    var name : String
    private(set) var loops : [Loop] = []
    
    private var loopData : [NSDictionary] = []
    private var loopsLoaded : Bool = false
    
    private init(name: String) {
        self.name = name
    }
    
    class func projectWithName(name: String) throws -> Project {
        let proj = Project(name: name)
        do {
            try proj.createLoopDirectory()
        }
        
        proj.writeToDisk()
        return proj
    }
    
    class func projectFromDictionary(dict: NSDictionary) throws -> Project {
        guard let name = dict.objectForKey(kProjectNameKey) as? String else {
            throw ProjectLoadError.MissingKey(name: kProjectNameKey)
        }
        
        let proj = Project(name: name)
        guard let loopData = dict.objectForKey(kLoopsKey) as? [NSDictionary] else {
            NSLog("No loop data exists for the project. Opening anyway...")
            return proj
        }
        
        proj.loopData = loopData
        
        do {
            try proj.createLoopDirectory()
        }
        
        proj.writeToDisk()
        
        return proj
    }
    
    func loadLoops(completion completion: () -> Void) {
        weak var weakSelf = self
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            for dict in self.loopData {
                guard let loop = Loop.loopFromDictionary(dict, project: self) else {
                    continue
                }
                
                self.loops.append(loop)
            }
            
            dispatch_async(dispatch_get_main_queue(), { 
                weakSelf!.loopsLoaded = true
                completion()
            })
        }
    }
    
    func unloadLoops() {
        self.writeToDisk()
        
        for loop in self.loops {
            loop.stop()
        }
        
        self.loops.removeAll()
        self.loopsLoaded = false
    }
    
    func addLoop(loop: Loop) {
        
        if self.loopsLoaded {
            self.loops.append(loop)
        }
        
        self.writeToDisk()
        unowned let unownedSelf = self
        NSNotificationCenter.defaultCenter().postNotificationName(kProjectUpdateNotification, object: unownedSelf)
    }
    
    func removeLoop(loop: Loop) {
        if self.loopsLoaded {
            if let loopIndex = self.loops.indexOf({$0 === loop}) {
                self.loops.removeAtIndex(loopIndex)
                self.writeToDisk()
            }
        }
        
        do {
            try NSFileManager.defaultManager().removeItemAtURL(Loop.urlForLoopWithFileName(loop.fileName(), inProject: self))
        } catch let err as NSError {
            NSLog("File removal error: \(err)")
        }
        
        unowned let unownedSelf = self
        NSNotificationCenter.defaultCenter().postNotificationName(kProjectUpdateNotification, object: unownedSelf)
        
        self.writeToDisk()
    }
    
    func removeAllLoops() {
        self.unloadLoops()
        for dict in self.loopData {
            do {
                let filepath = dict.objectForKey("filename") as! String
                try NSFileManager.defaultManager().removeItemAtURL(Loop.urlForLoopWithFileName(filepath, inProject: self))
            } catch let err as NSError {
                NSLog("File removal error: \(err)")
            }
        }
        
        unowned let unownedSelf = self
        NSNotificationCenter.defaultCenter().postNotificationName(kProjectUpdateNotification, object: unownedSelf)
    }
    
    func pathForLoopDirectory() -> String {
        let dirs = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as [String]
        
        let docsDir = dirs[0] as NSString
        return docsDir.stringByAppendingPathComponent(self.name.slug)
    }
    
    func projectFileName() -> String {
        return "\(self.name.slug).plist"
    }
    
    func pathForProjectFile() -> String {
        let searchDirs = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)
        let libraryDir = searchDirs[0] as NSString
        
        return libraryDir.stringByAppendingPathComponent(self.projectFileName())
    }
    
    private func createLoopDirectory() throws {
        let path = self.pathForLoopDirectory()
        
        if NSFileManager.defaultManager().fileExistsAtPath(path) {
            return
        }
        
        do {
            try NSFileManager.defaultManager().createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil)
        }
    }
    
    private func writeToDisk() {
        let path = self.pathForProjectFile()
        let dict = NSMutableDictionary()
        dict.setObject(self.name as NSString, forKey: kProjectNameKey)
        if self.loopsLoaded {
            self.updateLoopData()
        }
        
        let loopsArray = NSMutableArray()
        for l in self.loopData {
            loopsArray.addObject(l)
        }
        
        dict.setObject(loopsArray, forKey: kLoopsKey)
        
        dict.writeToFile(path, atomically: true)
    }
    
    private func updateLoopData() {
        self.loopData.removeAll()
        for loop in self.loops {
            self.loopData.append(loop.toDict())
        }
    }
}

enum ProjectLoadError : ErrorType {
    case MissingKey(name : String)
}

private let kLoopsKey = "loops"
private let kProjectNameKey = "name"
