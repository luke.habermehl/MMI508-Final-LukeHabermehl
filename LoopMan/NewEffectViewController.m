//
//  NewEffectViewController.m
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import "NewEffectViewController.h"
#import "AVAudioUnitEQ+AudioPlayer.h"

@interface NewEffectViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *effectPicker;
@property (nonatomic) NSInteger selectedIndex;

@end

@implementation NewEffectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupBarButtons];
    
    [self.effectPicker setDelegate:self];
    [self.effectPicker setDataSource:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)submitButtonPressed
{
    AVAudioUnit *audioUnit;
    
    switch (self.selectedIndex)
    {
        case 0:
            audioUnit = [AVAudioUnitEQ defaultFourBandEqualizer];
            break;
            
        case 1:
            audioUnit = [[AVAudioUnitReverb alloc] init];
            break;
            
        case 2:
            audioUnit = [[AVAudioUnitDelay alloc] init];
            break;
            
        default:
            return;
    }
    
    [self.delegate didCreateNewEffect:audioUnit];
}

- (void)cancelButtonPressed
{
    [self.delegate didCancelCreateNewEffect];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedIndex = row;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (row)
    {
        case 0:
            return NSLocalizedString(@"equalizer", nil);
        case 1:
            return NSLocalizedString(@"reverb", nil);
        case 2:
            return NSLocalizedString(@"delay", nil);
    }
    
    return nil;
}

#pragma mark - Private

- (void)setupBarButtons
{
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(submitButtonPressed)];
    
    self.navigationItem.rightBarButtonItem = submitButton;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed)];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
}

@end
