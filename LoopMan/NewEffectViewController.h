//
//  NewEffectViewController.h
//  LoopMan
//
//  Created by Luke on 4/12/16.
//  Copyright © 2016 Luke Habermehl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol NewEffectViewControllerDelegate

- (void)didCreateNewEffect:(AVAudioUnit *)audioUnit;
- (void)didCancelCreateNewEffect;

@end

@interface NewEffectViewController : UIViewController

@property (weak, nonatomic) id<NewEffectViewControllerDelegate> delegate;

@end
